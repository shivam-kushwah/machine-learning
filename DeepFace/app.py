from deepface import DeepFace
import cv2

cam = cv2.VideoCapture(0)
cv2.namedWindow("Test")

img_counter = 0

while True:
  ret, frame = cam.read()

  if not ret:
    print("failed to grab frame")
    break
  
  
  try:
    df = DeepFace.find(img_path = frame, db_path = "/home/shivam/Downloads/DeepFace/dataset", model_name = "VGG-Face")
    name = df['identity'][0].split("/")[-2]
    print(">>>>>>>>>>>>>>>>>>>>>>>",name)
    cv2.putText(frame, name, (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

    print(df)
  except:
    print("Not able to detect")
  cv2.imshow("Test",frame)
  k = cv2.waitKey(1)

  if k%256 == 27:
    print("Escape hit!")
    break

cam.release()

cam.destroyAllWindows()
